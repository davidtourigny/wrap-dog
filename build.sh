set -eux

# Use a default of one core.
: "${NPROC:=1}"

build_dir=build

mkdir "${build_dir}"
cd "${build_dir}"

if [[ "$OSTYPE" == "linux-gnu" || "$OSTYPE" == "linux" ]]; then
cmake -DCMAKE_BUILD_TYPE=DEBUG ..
elif [[ "$OSTYPE" == "darwin"* ]]; then
cmake -DCMAKE_BUILD_TYPE=DEBUG ..
fi
make -j "${NPROC}"
cd ..
