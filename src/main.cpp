#include "Dog.h"
#include <boost/python.hpp>

#if PY_MAJOR_VERSION >= 3
#   define PATH L"."
#else
#   define PATH (char*)"."
#endif

int main(int arg, char** argv)
{
    try
    {
        Py_Initialize();
        boost::python::object main_module = boost::python::import("__main__");
        boost::python::object main_namespace = main_module.attr("__dict__");
        PySys_SetPath(PATH); //if module not installed
        main_namespace["DogModule"] = boost::python::import("DogModule"); //import module to namespace
        boost::python::object dog_py = boost::python::eval("DogModule.DogType('harry')", main_namespace); //instantiate python object
        std::string speech = boost::python::extract<std::string>(dog_py.attr("bark")("woof")); //test python object
        std::cout << speech << std::endl; //print
        boost::python::extract<Dog&> dog(dog_py); //extract python object
        if(dog.check()){ //check extraction to type was successful
            Dog &dog_cpp = dog(); //convert to c++ object
            speech = dog_cpp.bark("woof"); //test c++ object
            std::cout << speech << std::endl; //print
        }
    }
    catch (const boost::python::error_already_set&)
    {
        PyErr_Print();
        return 1;
    }
}
