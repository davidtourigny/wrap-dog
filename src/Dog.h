#include <stdlib.h>
#include <string>
#include <iostream>

struct Dog
{
    Dog(std::string text) { name = text; };
    std::string bark(std::string text) { return "The dog called " + name + " barks: " + text; }
    std::string name;
};

