#include "Dog.h"
#include <boost/python.hpp>

BOOST_PYTHON_MODULE(DogModule)
{
    boost::python::class_<Dog>("DogType", boost::python::init<std::string>())
    .def("bark", &Dog::bark)
    ;
}

